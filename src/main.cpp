#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <string>
#include <argp.h>
#include "calcul.h"
#include "affichage.h"
#include "gestion_cli.h"

using namespace std;

int main(int nombre_de_parametres, char **tableau_des_parametres)
{
  struct arguments mes_args = appelArgp(nombre_de_parametres, tableau_des_parametres);
  PGconn *retour_connexion = PQsetdbLogin(mes_args.host,
                                          "5432",
                                          nullptr,
                                          nullptr,
                                          mes_args.dbname,
                                          mes_args.user,
                                          mes_args.password);
  PGresult *requete = PQexec(retour_connexion, "SET SCHEMA 'si6'; SELECT \"Animal\".id,\"Animal\".nom,sexe,date_naissance AS \"date de naissance\" ,commentaires,\"Race\".nom as race, description FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';");
  unsigned int nbresult = PQntuples(requete);
  unsigned int nbcolonnes = PQnfields(requete);
  ExecStatusType reussite = PQresultStatus(requete);
  char *status_requete = PQresStatus(reussite);

  if(PQstatus(retour_connexion) != CONNECTION_OK)
  {
    std::cerr << "La connexion au serveur de base de données '" << PQhost(retour_connexion) << "' à échoué." << std::endl;
  }
  else
  {
    AfficheInfos(retour_connexion);

    //Question 4 et 6

    if(reussite == PGRES_TUPLES_OK)
    {
      unsigned int caractere = 0, nbrSeparation = 0;
      unsigned int nbrCaractere = calculNombreCaractere(caractere, nbcolonnes, requete);
      unsigned int nbrSeparationTiret  = separationTiret(nbcolonnes, nbrSeparation, nbrCaractere, requete);
      affichage_tiret(nbrSeparationTiret);
      std::cout << endl <<  "| ";
      affichage_entete(requete, nbrCaractere, nbcolonnes);
      std::cout << std::endl;
      affichage_tiret(nbrSeparationTiret);
      std::cout << std::endl;
      affichage_donnees(requete, nbrCaractere, nbcolonnes, nbresult);
      affichage_tiret(nbrSeparationTiret);
      std::cout << "\nL'exécution de la requête SQL a retourné " << nbresult << " enregistrements." << std::endl;
    }
    else
    {
      AfficheErreur(reussite, status_requete);
    }
  }

  return 0;
}
